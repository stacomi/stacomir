copier_option <- function(
  file_path = "chemin/vers/votre/fichier.txt",
  old_text = "# options here for codecov",
  new_text = "mon code"
) {
  # Lire le contenu du fichier
  file_content <- readLines(file_path, encoding = "UTF-8")

  # Remplacer le texte
  new_content <- gsub(old_text, new_text, file_content)

  # Écrire le nouveau contenu dans le fichier
  writeLines(new_content, file_path)

  cat("Remplacement effectué avec succès dans le fichier :", file_path, "\n")
}
