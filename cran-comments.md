# Stacomir0.6.1

## RESUBMISSION

This is a new release of stacomiR, it fixes issues on CRAN and several bugs,
with developments for testing using a minimal test database in postgres (https://forgemia.inra.fr/stacomi/stacomi_db)
and support for a shiny interface (https://forgemia.inra.fr/stacomi/stacoshiny).
I was not able to fix the issues on time to resubmit to CRAN
by february this year.


## TESTS

Passing all tests on my local machine (windows) R4.1.2 (requires a database installed)
   
* [ FAIL 0 | WARN 0 | SKIP 0 | PASS 293 ]
* code coverage : 84 %

## CHECKS

With no error, no warnings, and notes either related to package build on different systems 
or that this is a new submission


* Windows r-4.3.1 (my local machine) 
0 errors ✔ | 0 warnings ✔ | 0 note ✔


* windows x86_64 (patched) (rforge)  
 0 errors ✔ | 0 warnings ✔ | 1 note ✖
 
Maintainer: 'Cedric Briand '
New submission

* Windows Server 2022, R-devel, 64 bit (rhub)
 0 errors ✔ | 0 warnings ✔ | 3 notes ✖

  * checking HTML version of manual ... [32s] NOTE
     Skipping checking math rendering: package 'V8' unavailable
  * checking for non-standard things in the check directory ... NOTE
     Found the following files/directories:
    ''NULL''
  * checking for detritus in the temp directory ... NOTE
    Found the following files/directories:
    'lastMiKTeXException'
 These issues are related to server installation : https://github.com/r-hub/rhub/issues/548

* Fedora Linux, R-devel (rhub)
 0 errors ✔ | 0 warnings ✔ | 1 note ✖
 
 NOTE
  Skipping checking HTML validation: no command 'tidy' found
  Skipping checking math rendering: package 'V8' unavailable
 

* Linux x86_64 (patched)  (rforge)
0 errors ✔ | 0 warnings ✔ | 1 note ✖

   Maintainer: ‘Cedric Briand ’
   New submission
   Package was archived on CRAN

* Ubuntu Linux 20.04.1 LTS, R-release, GCC
0 errors ✔ | 0 warnings ✔ | 1 note ✖

   * checking HTML version of manual ... NOTE
    Skipping checking HTML validation: no command 'tidy' found
    Skipping checking math rendering: package 'V8' unavailable

 * r-release-macosx-arm64|4.3.0|macosx|macOS 13.3.1
 0 errors ✔ | 1 warnings ✖ | O note 
 
 using check arguments '--no-clean-on-error '




